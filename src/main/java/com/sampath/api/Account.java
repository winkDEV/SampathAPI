package com.sampath.api;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection="Accounts")
public class Account {

	@Id
	private String id;
	private String ccnumber;
	private String name;
	private double amount;
	private int cvcnumber;
	
	
	public String getCcnumber() {
		return ccnumber;
	}
	public String getName() {
		return name;
	}
	public double getAmount() {
		return amount;
	}
	public int getCvcnumber() {
		return cvcnumber;
	}
	/*
	public Account(String ccnumber, String name, double amount, int cvcnumber) {
		//super();
		this.ccnumber = ccnumber;
		this.name = name;
		this.amount = amount;
		this.cvcnumber = cvcnumber;
	}
	*/
	public void setAmount(double amount) {
		this.amount = amount;
	}
	
	
	
	
}
