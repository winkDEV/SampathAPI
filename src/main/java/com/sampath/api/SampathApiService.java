package com.sampath.api;



public interface SampathApiService {
	
	Response getAccountByCCNumber(Request req);
	
	Response doPayment(Request req);

}
