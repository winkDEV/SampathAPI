package com.sampath.api;



import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin
@RestController
@RequestMapping("/sampath_api")
public class AccountController {

	
	private SampathApiServiceImpl sampathService;
	
	private final static Logger logger=LoggerFactory.getLogger(AccountController.class);

	@Autowired
	public AccountController(SampathApiServiceImpl sampathService) {
		//super();
		this.sampathService = sampathService;
	}
	
	
	@RequestMapping(value="/accounts/{ccnumber}",method=RequestMethod.GET,produces = "application/json",consumes="application/json")
	public Response getAccountByCCNumber(@PathVariable("ccnumber") String req) {
		
		
		Response res=this.sampathService.getAccountByCCNumber(new Request(req));
		
		
		return res;
	}
	
	
	@RequestMapping(value="/accounts",method=RequestMethod.POST,consumes=MediaType.APPLICATION_JSON_VALUE,produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Response> doPayment(@RequestBody Request req) {
		
		logger.info("POST Request handling");
		//Request req=requestEntity.getBody();
		Response res=this.sampathService.doPayment(req);
		
		if(res.isStatus()) {
			return new ResponseEntity<Response>(res, HttpStatus.OK);
		}
		
		else {
			return new ResponseEntity<Response>(res, HttpStatus.BAD_GATEWAY);
		}
			
		
		
		
	}
	
	
	
	
	/*
	@RequestMapping(method=RequestMethod.POST)
	public void getAccountByName(@RequestBody Account account) {
		//this.accountRepositary.save(account);
		
		//return account;
	}
	*/
	
	/*
	@GetMapping("/all")
	public List<Account> getAll(){
		List<Account> accounts=this.accountRepositary.findAll();
		return accounts;
	}
	*/
}
