package com.sampath.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SampathApiServiceImpl implements SampathApiService {
	
	
	private AccountRepositary accountRepositary;
	
	
	@Autowired
	public SampathApiServiceImpl(AccountRepositary accountRepositary) {
		
		this.accountRepositary = accountRepositary;
	}

	@Override
	public Response getAccountByCCNumber(Request req) {
		
		Account account=this.accountRepositary.findByccnumber(req.getCcNumber());
		
		return new Response(true,"Account Exists");
	}

	@Override
	public Response doPayment(Request req) {
		
		Account account=this.accountRepositary.findByccnumber(req.getCcNumber());
		
		if(account!=null) {
			double amount=account.getAmount();
			account.setAmount(amount+req.getAmount());
			
			this.accountRepositary.save(account);
			
			return new Response(account.getName(),account.getAmount(),true,"Payment Successfull");
		}
		else {
			return new Response(false,"Payment Unsuccessfull");
		}
		
		
	}

}
