package com.sampath.api;





import java.util.Optional;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.sampath.api.Account;

@Repository
public interface AccountRepositary extends MongoRepository<Account, String>{

	
	Account findByccnumber(String ccnumber);

	//boolean existByccnumber(String ccnumber);
	
	//Account findByname(String name);
}
