package com.sampath.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
public class SampathApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(SampathApiApplication.class, args);
	}
}
